<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('main.index');
//});

Route::get('/',[\App\Http\Controllers\Main\maincontroller::class,'index'])->name('main.index');
Route::get('/search',[\App\Http\Controllers\Main\maincontroller::class,'search'])->name('search');
Route::get('/detail/{book}/',[\App\Http\Controllers\Main\maincontroller::class,'book_detail'])->name('book.detail');
Route::get('/category/{id}',[\App\Http\Controllers\Main\maincontroller::class,'category_filter'])->name('category.filter');
Route::get('/author/{id}',[\App\Http\Controllers\Main\maincontroller::class,'author_filter'])->name('author.filter');
Route::get('/card/{id}',[\App\Http\Controllers\Main\maincontroller::class,'added_cards'])->name('added.cards');
Route::get('/view_card',[\App\Http\Controllers\Main\maincontroller::class,'view_card'])->name('view.card');
Route::get('/cancel/{id}',[\App\Http\Controllers\Main\maincontroller::class,'cancel_book'])->name('cansel.book');
Route::get('/contact',[\App\Http\Controllers\Main\maincontroller::class,'contact'])->name('contact');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'admin'])->name('admin_panel');

    Route::resource('/category', \App\Http\Controllers\Admin\categoryController::class);

    Route::resource('/books', App\Http\Controllers\Admin\BookController::class);
});
//Route::group(['prefix' => 'User', 'middleware' => ['auth', 'user']], function () {
//    Route::get('/', [\App\Http\Controllers\HomeController::class, 'user'])->name('userpanel');
//
//
//
//});

Route::post('/comment',[\App\Http\Controllers\CommentComtroller::class,'store'])->name('comment')->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::post('/payment',[\App\Http\Controllers\ChecoutController::class,'session'])->name('data');
Route::get('/success',[\App\Http\Controllers\ChecoutController::class,'success'])->name('success');
