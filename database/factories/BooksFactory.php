<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BooksFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'book_name'=>$this->faker->name,
            'book_author'=>$this->faker->name,
            'category_id'=>rand(1,11),
            'book_prise'=>rand(25000,95000),
            'book_images'=>'image.jpeg',
            'book_description'=>$this->faker->text(665),




        ];
    }
}
