<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['category_name'=>'Badiiy']);
        Category::create(['category_name'=>'Romanlar']);
        Category::create(['category_name'=>'Hikoyalar va Qisqacha Hikoyalar']);
        Category::create(['category_name'=>'Ilmiy fantastika va Fantastika']);
        Category::create(['category_name'=>'Romantika']);
        Category::create(['category_name'=>'Siyosat va Jamiyat']);
        Category::create(['category_name'=>'Din va Ruhoniyat']);
        Category::create(['category_name'=>'Falsafa']);
        Category::create(['category_name'=>'San\'at va musiqa']);
        Category::create(['category_name'=>'Moliya va Sarmoyalar']);
        Category::create(['category_name'=>'Psixologiya va Ma\'naviyat Ilmi']);


    }
}
