<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\Card;
use App\Models\Commente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class maincontroller extends Controller
{
    public function index()
    {

//        $image_for_view = [];
////        $product_types= DB::select('select model_type, count(model_type) as count from products where status=1 & block=1 group by model_type ');
//        $products_accepts = Product::where('status','=',1)->where('block','=',1)->get();
//        for ($i=0; $i<count($products_accepts); $i++)
//        {
//            $medium_img_array = explode(',',$products_accepts[$i]->model_image_medium);
//            $image_for_view[$i] = $medium_img_array[0];
//        }
//        return view('main/index', compact('product_types','products_accepts','image_for_view'));


        $img_for_view = [];
//        $categories = Category::all();
        $books = Books::orderBy('id', 'desc')->paginate('6');
//        $books = Books::orderBy('id','desc')->get();
        for ($i = 0; $i < count($books); $i++) {
//            $img_for_view = [];
            $img_array = explode('<!', $books[$i]->book_images);
            $img_for_view[$i] = $img_array[0];

        }

        return view('main.index', compact('books', 'img_for_view'));
    }

    public function book_detail(Books $book)
    {
        $comments = Commente::where('book_id', $book->id)->get();


        $book_img = explode('<!', $book->book_images);
        return view('main.detail', compact('book', 'book_img', 'comments'));
    }


    public function category_filter($id)
    {

        $img_for_view = [];
        $books = Books::where('category_id', '=', $id)->paginate('6');
        for ($i = 0; $i < count($books); $i++) {
            $img_array = explode('<!', $books[$i]->book_images);
            $img_for_view[$i] = $img_array[0];

        }

        return view('main.index', compact('books', 'img_for_view'));

    }

    public function author_filter($id)
    {
        $img_for_view = [];
        $books = Books::where('book_author', '=', $id)->paginate('6');
        for ($i = 0; $i < count($books); $i++) {
            $img_array = explode('<!', $books[$i]->book_images);
            $img_for_view[$i] = $img_array[0];

        }

        return view('main.index', compact('books', 'img_for_view'));
    }


    protected function added_cards($id)
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $book_id = $id;
            $test = Card::query()->where('user_id', '=', $user_id)->where('book_id', '=', $book_id);
//            dd($test);
            if (!$test->exists()) {

                $card = new Card();
                $card->user_id = $user_id;
                $card->book_id = $book_id;
                $card->save();

//                dd('salom');
//                $card = Card::find(Card::query()->where('user_id', '=', $user_id)->where('book_id', '=', $book_id)->get()[0]['id']);
//                $card->book_qty = $card->book_qty + 1;
//                $card->update();
//            } else {
////                dd('alek');
//                $card = new Card();
//                $card->user_id = $user_id;
//                $card->book_id = $book_id;
////                $card->book_qty = 1;
//                $card->save();
            }

            return redirect()->back();


        } else {
            return redirect()->route('login');
        }


    }

    public function view_card()
    {

        if (Auth::check()) {

            $user_cards = Card::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();

            return view('main.card', compact('user_cards'));

//            $img_for_view = [];
//            $cards = Card::where('book_id', '=', $id)->paginate('6');
//            for ($i = 0; $i < count($books); $i++) {
//                $img_array = explode('<!', $books[$i]->book_images);
//                $img_for_view[$i] = $img_array[0];


        } else {
            return redirect()->route('login');
        }
    }


    public function cancel_book(Auth $user, $user_card)

    {


        Card::where('user_id', '=', Auth::user()->id)
            ->where('book_id', '=', $user_card)->delete();
         return redirect()->back();





    }

    public function contact()
    {
        return view('main.contact');
    }

//    public function search(Request $request)
//    {
//        if ($request->ajax()) {
//            $query = $request->get('query');
//            if ($query != '') {
//                $data = \DB::table('books')
//                    ->where('book_name', 'like', '%' . $query . '%')
//                    ->orWhere('book_author', 'like', '%' . $query . '%')
//                    ->orWhere('category_id', 'like', '%' . $query . '%')
//                    ->orWhere('book_prise', 'like', '%' . $query . '%')
//                    ->orWhere('book_description', 'like', '%' . $query . '%')
//                    ->orderBy('id', 'desc')
//                    ->get();
////                return redirect()->route('main.index');
//                \Log::alert($query);
//
//            } else {
//                $data = \DB::table('books')->orderBy('id', 'desc')->get();
////                return redirect()->route('main.index');
//            }
//
//
//
////            echo json_encode($data);
//
//            return response()->json($data);
//
//        }
//    }







    public function search(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');
            if ($query != '') {
                $data = \DB::table('books')
                    ->where('book_name', 'like', '%' . $query . '%')
                    ->orWhere('book_author', 'like', '%' . $query . '%')
                    ->orWhere('category_id', 'like', '%' . $query . '%')
                    ->orWhere('book_prise', 'like', '%' . $query . '%')
                    ->orWhere('book_description', 'like', '%' . $query . '%')
                    ->orderBy('id', 'desc')
                    ->get();

                \Log::alert($data);


            } else {
                $data = \DB::table('books')->orderBy('id', 'desc')->get();
            }

            return response()->json($data);
        }
    }



}

