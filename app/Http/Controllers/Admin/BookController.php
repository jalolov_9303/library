<?php

namespace App\Http\Controllers\Admin;

use App\Events\BookEvent;
use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\Category;
use App\Models\Commente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $books = Books::orderBy('id', 'desc')->get();
//        $book_img = explode('<!',$books->book_images);
        $title = 'Delete book!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        return view('admin.book.view', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.book.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'book_name' => 'required',
            'book_author' => 'required',
            'category_id' => 'required',
            'book_prise' => 'required',
            'book_img' => 'required|array',
            'book_description' => 'required',

        ]);

        $book_name = $request->book_name;
        $book_author = $request->input('book_author');
        $category_id = $request->input('category_id');
        $book_prise = $request->input('book_prise');
        $book_description = $request->input('book_description');

        $images = '';

        for ($i = 0; $i < count($request->file('book_img')); $i++) {
            $all_img = $request->file('book_img')[$i];
            $extension = $all_img->getClientOriginalExtension();
            $fileNameToStore = time();
            $images .= $fileNameToStore . $i . '.' . $extension . '<!';
            $all_img->move(public_path('admin/book_images/'), $fileNameToStore . $i . '.' . $extension);
        }
        $books = new Books();
        $books->book_name = $book_name;
        $books->book_author = $book_author;
        $books->category_id = $category_id;
        $books->book_prise = $book_prise;
        $books->book_images = $images;
        $books->book_description = $book_description;
        $books->save();
        event(new BookEvent('hello world'));
//        event(new BookEvent("You created successfully"));
        Alert::success('Success Title', 'Book created successfully');
//        event(new BookEvent($books) );
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Books $book)
    {

        $book_img = explode('<!', $book->book_images);
        return view('admin.book.show', compact('book', 'book_img'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Books $book)
    {
        $categories = Category::all();
        $book_img = explode('<!', $book->book_images);
        return view('admin.book.edit', compact('book', 'categories', 'book_img'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Books $book)
    {


        $request->validate([
            'book_name' => 'required',
            'book_author' => 'required',
            'category_id' => 'required',
            'book_prise' => 'required',
//                'book_img' => 'required|array',
            'book_description' => 'required',

        ]);

        $book_name = $request->book_name;
        $book_author = $request->input('book_author');
        $category_id = $request->input('category_id');
        $book_prise = $request->input('book_prise');
        $book_description = $request->input('book_description');

        if ($request->hasFile('book_img')) {
            $old_img = explode('<!', $book->book_images);
            for ($i = 0; $i < count($old_img) - 1; $i++) {
                File::delete(public_path('admin/book_images/' . $old_img[$i]));
            }


            $images = '';

            for ($i = 0; $i < count($request->file('book_img')); $i++) {
                $all_img = $request->file('book_img')[$i];
                $extension = $all_img->getClientOriginalExtension();
                $fileNameToStore = time();
                $images .= $fileNameToStore . $i . '.' . $extension . '<!';
//                    $org_img = $fileNameToStore . $i . '.' . $extension.'<!';
//                    array_push($images, $org_img);
                $all_img->move(public_path('admin/book_images/'), $fileNameToStore . $i . '.' . $extension);
            }

        } else {
            $images = $book->book_images;
        }

//            $images = implode($images,'<!');
//            $books = new Books();
        $book->book_name = $book_name;
        $book->book_author = $book_author;
        $book->category_id = $category_id;
        $book->book_prise = $book_prise;
        $book->book_images = $images;
        $book->book_description = $book_description;
        $book->update();
        Alert::success('Success Title', 'Book changed successfully');
        return redirect()->route('books.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Books $book)
    {
        $old_img = explode('<!', $book->book_images);
        for ($i = 0; $i < count($old_img) - 1; $i++) {
            File::delete(public_path('admin/book_images/' . $old_img[$i]));
        }
        $book->delete();

//
        Alert::success('Success Title', 'Book deleted successfully');
        return redirect()->back();

    }
}
