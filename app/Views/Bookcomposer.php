<?php

namespace App\Views;

use App\Models\Books;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class Bookcomposer
{

    public function compose(View $view)
    {

        $book_author_counts = Books::query()
            ->select( 'book_author', DB::raw('count(book_author) as count'))
            ->groupBy('book_author')
            ->get();;
        $view->with([
            'book_author_counts' => $book_author_counts
        ]);
    }

}
