<?php

namespace App\Views;

use App\Models\Books;
use App\Models\Category;
use Illuminate\View\View;

class Categorycomposer
{
    public  function compose(View $view)
    {
        $category_count=[];
        $categories = Category::all();
        for ($i=0; $i<count($categories); $i++)
        {
            $count_category = Books::where('category_id','=',$categories[$i]->id)->count();
            $category_count[$i]=$count_category;
        }

        $view->with([
            'categories'=>$categories,
            'category_count'=>$category_count
        ]);
    }

}
