<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;

    protected $fillable = [
        'book_name',
        'book_author',
        'category_id',
        'book_prise',
        'book_images',
        'book_description',
    ];


    public function category_name()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(Commente::class);
    }

    public  function user()
    {
        return $this->belongsTo(User::class);
    }


}
