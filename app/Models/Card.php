<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'book_id',
        'book_qty',
    ];

    public function user_name()
    {
        return $this->belongsTo(Auth::class, 'user_id', 'id');
    }

    public function book(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Books::class, 'book_id', 'id');
    }


}
