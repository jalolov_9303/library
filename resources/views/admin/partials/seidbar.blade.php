<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="index.html"><img class="img-fluid for-light" src="{{asset('admin/assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="../assets/images/logo/logo_dark.png" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="{{asset('admin/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <a href="{{route('main.index')}}"> Main blade</a>
                        </div>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="book"></i><span >Category </span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/category/create') || request()->is('dashboard/category')) || request()->is('dashboard/category/*/edit')  ? 'd-block': ''}}">
                            <li><a href="{{route('category.create')}}" style="color: {{request()->is('dashboard/category/create')? '#6600FF': ''}}">Add new category</a></li>
                            <li><a href="{{route('category.index')}}" style="color: {{request()->is('dashboard/category')? '#6600FF': ''}}">View category</a></li>
                        </ul>

                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="book"></i><span >Book</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/books/create') || request()->is('dashboard/books')) || request()->is('dashboard/books/*/edit')  ? 'd-block': ''}}">
                            <li><a href="{{route('books.create')}}" style="color: {{request()->is('dashboard/books/create')? '#6600FF': ''}}">Add new book</a></li>
                            <li><a href="{{route('books.index')}}" style="color: {{request()->is('dashboard/books')? '#6600FF': ''}}">View books</a></li>
                        </ul>

                    </li>



                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
