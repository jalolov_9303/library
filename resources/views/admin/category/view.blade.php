@extends('admin.layouts.master')
@section('title','View category')
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>All categories</h5>
                <a href="{{route('category.create')}}" class="btn btn-primary">create category</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Category title</th>
                            <th>Added date</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $key=>$category)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$category->category_name}}</td>
                                <td>{{$category->created_at}}</td>


                                <td class="d-flex">
                                    <a href="{{route('category.edit',$category)}}" class="btn btn-success">Edit</a>

{{--                                    <form action="{{route('category.destroy',$category->id)}}" method="POST"--}}
{{--                                          onsubmit="return confirm('Rostdan ham {{$category->category_name}} o\'chirishni xoxlaysizmi  ? ')">--}}
{{--                                        @method('Delete')--}}
{{--                                        @csrf--}}
{{--                                        <button class=" btn btn-sm btn-danger show_confirm" type="submit "><i--}}
{{--                                                class="fa fa-trash-o"></i>--}}
{{--                                        </button>--}}
{{--                                    </form>--}}

                                    <a href=" {{route('category.destroy',$category->id)}}" class="btn btn-danger" data-confirm-delete="true">Delete</a>


                                    {{--                                    &nbsp;<form action="{{route('category.destroy',$category->id)}}" method="post">--}}
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <button type="submit" class="btn btn-danger show_confirm">Delete</button>--}}
{{--                                    </form>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
