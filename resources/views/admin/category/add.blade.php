@extends('admin.layouts.master');
@section('title','Add category')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Add
                                    category</font></font></span></h5>
                    </div>

                    <div class="card-body">
                        <form class="form-wizard" id="regForm" action="{{route('category.store')}}" method="POST">
                            @csrf
                            <div class="tab" style="display: block;">
                                <div class="mb-3">
                                    <label for="name"><font style="vertical-align: inherit;"><font
                                                style="vertical-align: inherit;">Category name</font></font></label>
                                    <input name="category_name"
                                           class="form-control  @error('category_name') is-invalid @enderror" id="name"
                                           type="text" placeholder="" required="required" data-bs-original-title=""
                                           title="">
                                </div>
                                @error('category_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <br>
                            <div>
                                <div class="text-end btn-mb">
                                    <button type="submit" class="btn btn-success"> Add category</button>
                                </div>
                            </div>
                            <!-- Circles which indicates the steps of the form:-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
