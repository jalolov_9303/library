@include('sweetalert::alert')


    <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 5 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('admin/assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.png')}}" type="image/x-icon">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <title> @yield('title','Welcome' )</title>
    {{--    <title>Cuba - Premium Admin Template</title>--}}

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/font-awesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/feather-icon.css')}}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/date-picker.css')}}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('admin/assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/responsive.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/select2.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/datatables.css')}}">
</head>
<body>
<!-- tap on top starts-->
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<!-- tap on tap ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
    @include('admin.partials.navbar')
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        @include('admin.partials.seidbar')
        <!-- Page Sidebar Ends-->
        <div class="page-body">

            <!-- Container-fluid starts-->

            @yield('content')


            <!-- Container-fluid Ends-->
        </div>

    </div>
</div>
<!-- latest jquery-->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script src="{{asset('admin/assets/js/jquery-3.5.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('admin/assets/js/bootstrap/bootstrap.bundle.min.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('admin/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('admin/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- scrollbar js-->
<script src="{{asset('admin/assets/js/scrollbar/simplebar.js')}}"></script>
<script src="{{asset('admin/assets/js/scrollbar/custom.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('admin/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
<script src="{{asset('admin/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('admin/assets/js/chart/chartist/chartist.js')}}"></script>
<script src="{{asset('admin/assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
<script src="{{asset('admin/assets/js/chart/knob/knob.min.js')}}"></script>
<script src="{{asset('admin/assets/js/chart/knob/knob-chart.js')}}"></script>
<script src="{{asset('admin/assets/js/chart/apex-chart/apex-chart.js')}}"></script>
<script src="{{asset('admin/assets/js/chart/apex-chart/stock-prices.js')}}"></script>
<script src="{{asset('admin/assets/js/notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('admin/assets/js/dashboard/default.js')}}"></script>
<script src="{{asset('admin/assets/js/notify/index.js')}}"></script>
<script src="{{asset('admin/assets/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('admin/assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('admin/assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
<script src="{{asset('admin/assets/js/typeahead/handlebars.js')}}"></script>
<script src="{{asset('admin/assets/js/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('admin/assets/js/typeahead/typeahead.custom.js')}}"></script>
<script src="{{asset('admin/assets/js/typeahead-search/handlebars.js')}}"></script>
<script src="{{asset('admin/assets/js/typeahead-search/typeahead-custom.js')}}"></script>
<script src="{{asset('admin/assets/js/tooltip-init.js')}}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('admin/assets/js/script.js')}}"></script>
<script src="{{asset('admin/assets/js/theme-customizer/customizer.js')}}"></script>

{{--@include('sweetalert::alert')--}}


<script src="{{asset('admin/assets/js/select2/select2.full.min.js')}}"></script>
<script src="{{asset('admin/assets/js/select2/select2-custom.js')}}"></script>
<!-- login js-->
<!-- Plugin used-->
<script src="{{asset('admin/assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/assets/js/datatable/datatables/datatable.custom.js')}}"></script>

<script src="{{asset('admin/assets/js/editor/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('admin/assets/js/editor/ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('admin/assets/js/editor/ckeditor/styles.js')}}"></script>
<script src="{{asset('admin/assets/js/editor/ckeditor/ckeditor.custom.js')}}"></script>




<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('615d5ef372ccb05f0401', {
        cluster: 'ap2'
    });

    var channel = pusher.subscribe('book_channel');
    channel.bind('book_event', function(data) {
        alert(JSON.stringify(data));
    });
</script>










@stack('script')

</body>
</html>


