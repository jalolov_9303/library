@extends('admin.layouts.master')
@section('title','Views all books')
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5>All categories</h5>
                <a href="{{route('books.create')}}" class="btn btn-primary">create book</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Book name</th>
                            <th>Book author</th>
                            <th>Category title</th>
                            <th>Book img</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $key=>$book)


                            <tr>
                                <a href="#">
                                    <td>{{$key+1}}</td>
                                    <td>{{$book->book_name}}</td>
                                    <td>{{$book->book_author}}</td>
                                    <td>{{$book->category_name->category_name}}</td>
                                    <td>


                                        <img src="{{asset('admin/book_images/'.explode('<!',$book->book_images)[0])}}"
                                             style="border-radius: 50%; background: red; width: 40px; height: 40px"
                                             alt=""></td>
                                    {{--                                    <img src="{{asset('files/'.explode(',',$post->photo)[0])}}" alt="image">--}}


                                    <td class="d-flex">
                                        <a href="{{route('books.show',$book)}}" class="btn btn-info"><i
                                                class="fa fa-eye"></i></a> &nbsp;

                                        <a href="{{route('books.edit',$book)}}" class="btn btn-success"><i
                                                class="fa fa-pencil"></i></a>



                                        <a href="{{ route('books.destroy', $book) }}" class="btn btn-danger" data-confirm-delete="true">Delete</a>

{{--                                        <form action="{{route('books.destroy',$book)}}" method="POST"--}}
{{--                                              onsubmit="return confirm('Rostdan ham {{$book->book_name}} o\'chirishni xoxlaysizmi  ? ')">--}}
{{--                                            @method('Delete')--}}
{{--                                            @csrf--}}
{{--                                            <button class=" btn btn-sm btn-danger show_confirm" type="submit "><i--}}
{{--                                                    class="fa fa-trash-o"></i>--}}
{{--                                            </button>--}}
{{--                                        </form>--}}

                                    </td>
                                </a>
                            </tr>


                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
