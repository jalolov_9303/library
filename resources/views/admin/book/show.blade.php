@extends('admin.layouts.master')
@section('title','Show book')
@section('content')

    <div class="row product-page-main p-0">
        <div class="col-xl-5 xl-100 box-col-6">
            <div class="card">
                <div class="card-body">
                    <div class="product-page-details">
                        <h3>{{$book->book_name}}</h3>
                    </div>
                    <div class="product-social"><b>Adding time :&nbsp;&nbsp;&nbsp;</b>{{substr($book->created_at,0,10)}}</div>
                    <ul class="product-color">
                        <li class="bg-primary"></li>
                        <li class="bg-secondary"></li>
                        <li class="bg-success"></li>
                        <li class="bg-info"></li>
                        <li class="bg-warning"></li>
                    </ul>
                    <hr>
                    <p>{!! $book->book_description !!}</p>
                    <hr>
                    <div>
                        <table class="product-page-width">
                            <tbody>
                            <tr>
                                <td> <b>Category :</b></td>
                                <td>{{$book->category_name->category_name}}</td>
                            </tr>
                            <tr>
                                <td> <b>Book name :</b></td>
                                <td>{{$book->book_name}}</td>
                            </tr>
                            <tr>
                                <td> <b>Book author :</b></td>
                                <td>{{$book->book_author}}</td>
                            </tr>

                            <tr>
                                <td> <b> Book Price :</b></td>
                                <td>{{$book->book_prise}} sum</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="product-title">share it</h6>
                        </div>
                        <div class="col-md-6">
                            <div class="product-icon">
                                <ul class="product-social">
                                    <li class="d-inline-block"><a href="#" data-bs-original-title="" title=""><i class="fa fa-facebook"></i></a></li>
                                    <li class="d-inline-block"><a href="#" data-bs-original-title="" title=""><i class="fa fa-google-plus"></i></a></li>
                                    <li class="d-inline-block"><a href="#" data-bs-original-title="" title=""><i class="fa fa-twitter"></i></a></li>
                                    <li class="d-inline-block"><a href="#" data-bs-original-title="" title=""><i class="fa fa-instagram"></i></a></li>
                                    <li class="d-inline-block"><a href="#" data-bs-original-title="" title=""><i class="fa fa-rss"></i></a></li>
                                </ul>
                                <form class="d-inline-block f-right"></form>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="product-title">Rate Now</h6>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex">
                                <div class="br-wrapper br-theme-fontawesome-stars"><select id="u-rating-fontawesome" name="rating" autocomplete="off" style="display: none;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select><div class="br-widget"><a href="#" data-rating-value="1" data-rating-text="1" class="br-selected br-current" data-bs-original-title="" title=""></a><a href="#" data-rating-value="2" data-rating-text="2" data-bs-original-title="" title=""></a><a href="#" data-rating-value="3" data-rating-text="3" data-bs-original-title="" title=""></a><a href="#" data-rating-value="4" data-rating-text="4" data-bs-original-title="" title=""></a><a href="#" data-rating-value="5" data-rating-text="5" data-bs-original-title="" title=""></a></div></div><span>&nbsp;<i data-feather="star"></i>&nbsp;<i data-feather="star"></i>&nbsp;<i data-feather="star"></i>&nbsp;<i data-feather="star"></i>&nbsp;<i data-feather="star"></i></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="m-t-15">
{{--                        <a class="btn btn-success m-r-10" href="{{asset('arch/models/files/'.$product->model_type.'/'.$product->model_file)}}"><i class="fa fa-download me-1"></i>Download 3D model</a>--}}

                    </div>
                </div>
            </div>
        </div>

        @for($i=0; $i<count($book_img)-1; $i++)
            <div class="col-xl-6 set-col-12 box-col-12">

{{--                <div class="col-sm-5"><img class="img-fluid sm-100-w" src="{{asset('admin/book_images/'.$book_img[$i])}}" alt=""></div>--}}


                <div class="card">
                    <div class="blog-box blog-shadow"><img class="img-fluid" src="{{asset('admin/book_images/'.$book_img[$i])}}" alt="">
                        <div class="blog-details">
                            <p>1024x720</p>
                        </div>
                    </div>
                </div>
            </div>
        @endfor




    </div>

@endsection
