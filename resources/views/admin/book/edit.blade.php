@extends('admin.layouts.master')
@section('title','Edit book')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Add
                                    Book</font></font></h5>
                    </div>
                    <div class="card-body">
                        <form class="form-wizard" id="regForm" action="{{route('books.update',$book)}}" method="POST"
                              enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="tab" style="display: block;">
                                <div class="mb-3">
                                    <label for="name"><font style="vertical-align: inherit;"><font
                                                style="vertical-align: inherit;">Book name</font></font></label>
                                    <input name="book_name" value="{{$book->book_name}}"
                                           class="form-control  @error('book_name') is-invalid @enderror" id="name"
                                           type="text" placeholder="" required="required" data-bs-original-title=""
                                           title="">
                                </div>
                                @error('book_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="tab" style="display: block;">
                                <div class="mb-3">
                                    <label for="name"><font style="vertical-align: inherit;"><font
                                                style="vertical-align: inherit;">Book author</font></font></label>
                                    <input name="book_author" value="{{$book->book_author}}"
                                           class="form-control  @error('book_author') is-invalid @enderror" id="name"
                                           type="text" placeholder="" required="required" data-bs-original-title=""
                                           title="">
                                </div>
                                @error('book_author')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="" style="display: block;">
                                <div class="mb-3">
                                    <div class="mb-3">
                                        <label class="form-label" for="exampleFormControlSelect9"> Select
                                            category</label>

                                            <select   name="category_id" class="form-select digits" id="exampleFormControlSelect9">

                                                @foreach($categories as $category)
                                                    @if($book->category_id == $category->id)
                                                        <option selected value="{{$category->id}}">{{$category->category_name}}</option>
                                                    @else
                                                        <option  value="{{$category->id}}">{{$category->category_name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                    </div>


                                </div>
                            </div>
                            <br>
                            <div class="tab" style="display: block;">
                                <div class="mb-3">
                                    <label for="name"><font style="vertical-align: inherit;"><font
                                                style="vertical-align: inherit;">Book prise</font></font></label>
                                    <input name="book_prise" value="{{$book->book_prise}}"
                                           class="form-control  @error('book_prise') is-invalid @enderror" id="name"
                                           type="number" placeholder="" required="required" data-bs-original-title=""
                                           title="">
                                </div>

                                <br>
                                @error('book_prise')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            @for($i=0; $i<count($book_img)-1; $i++)

                                <img style="width: 100px; height: 100px" src="{{ asset('admin/book_images/'.$book_img[$i])}}" alt="">
                            @endfor
{{--                            @foreach(explode('<!',$book->book_images) as $image)--}}
{{--                                <img style="width: 100px; height: 100px" src="{{ asset('admin/book_images/'.$image)}}" alt="">--}}
{{--                          &nbsp--}}
{{--                            @endforeach--}}

{{--                            @for($i=0; $i<count($book->book_images); $i++)--}}
{{--                                 <img src="{{asset('admin/book_images/'.explode('<!',$book->book_images)[$i])}}" style="width: 80px; height: 80px; background: red " alt="">--}}
{{--                            @endfor--}}

                            <div class="tab" style="display: block;">
                                <div class="mb-3">
                                    <label for="name"><font style="vertical-align: inherit;"><font
                                                style="vertical-align: inherit;">Book image</font></font></label>
                                    <input multiple name="book_img[]"
                                           class="form-control   @error('book_img') is-invalid @enderror" id="name"
                                           type="file" placeholder=""  data-bs-original-title=""
                                           title="">
                                </div>

                                <br>
                                @error('book_img')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div>
                                <label for="exampleFormControlTextarea1" class="form-label">Model description</label>
                                <textarea class="@error('book_description') is-invalid @enderror" id="editor1"
                                          name="book_description" cols="30" rows="10">{!! $book->book_description !!}</textarea>
                                @error('book_description')
                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                @enderror

                            </div>

                            <br>
                            <div class="text-end btn-mb">
                                <button type="submit" class="btn btn-success"> Update book</button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
