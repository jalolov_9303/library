<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shopping card</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
          rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('main/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/style.css')}}" type="text/css">
</head>

<body>

@include('main.partials.navbar')


<!-- Header Section End -->


<!-- Shopping Cart Section Begin -->
<section class="shopping-cart spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shopping__cart__table ">

                    <table>
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Narxi</th>
{{--                            <th>Soni</th>--}}
                            <th>Sotib olish</th>
                            <th>Bekor qilish</th>
                            <th></th>
                        </tr>
                        </thead>
                        @foreach($user_cards as $key=>$user_card)
                            <tbody>
                            <tr>
                                <td class="product__cart__item">
                                    <div class="product__cart__item__pic">

                                        {{--                                        @for($i=0; $i<count('{{$user_card->book->book_images}}'); $i++)--}}
                                        <img style="width:100px; height: 100px; "
                                             src="{{asset('admin/book_images/'.explode('<!',$user_card->book->book_images)[0])}}"
                                             alt="">
                                    </div>
                                    <div class="product__cart__item__text">
                                        <h6>{{$user_card->book->book_name}}</h6>
                                        <h5>{{$user_card->book->book_author}}</h5>
                                    </div>
                                </td>

                                <td class="cart__price"><input style="border: none; " id="price_{{$key}}"
                                                               value="{{$user_card->book->book_prise}} so'm"
                                                               disabled>
                                </td>

{{--                                <td class="quantity__item">--}}
{{--                                    <div style="display: flex; justify-content: start; flex-direction: row-reverse;">--}}
{{--                                        <button type="submit" onclick="plus({{$key}})"><i class="fa fa-plus"></i>--}}
{{--                                        </button>--}}
{{--                                        <input style="width: 40px" disabled id="quantity_{{$key}}" type="text"--}}
{{--                                               value="{{$user_card->book_qty}}">--}}
{{--                                        <button onclick="minus({{$key}})"><i class="fa fa-minus"></i></button>--}}
{{--                                    </div>--}}
{{--                                </td>--}}

{{--                                <td class="cart__price"><input id="total_{{$key}}" disabled style="border: none"--}}
{{--                                                               value="{{$user_card->book->book_prise*$user_card->book_qty}} so'm">--}}
{{--                                </td>--}}






                                <td>


                                    <form action="{{route('data')}}" method="Post">
                                        @csrf

                                        <input type="hidden" name="user_name" value="{{\Illuminate\Support\Facades\Auth::user()->name }}">
                                        <input type="hidden" name="price" value="{{$user_card->book->book_prise}}">
                                        <input type="hidden" name="productname" value="{{$user_card->book->book_name}}" >
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-success" type="submit">Sotib olish</button>
                                    </form>

                                </td>
                                <td class="cart__close"><a href="{{route('cansel.book',$user_card->book_id)}}"><i
                                            class="fa fa-close"></i></a></td>

                                <hr>

                            </tr>


                            </tbody>



                        @endforeach
                    </table>

                </div>

            </div>

        </div>
    </div>
</section>
<!-- Shopping Cart Section End -->

<!-- Footer Section Begin -->
<footer class="footer">
    <div class="container">
        
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="footer__copyright__text">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <p>Copyright ©
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        2020
                        All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                            aria-hidden="true"></i> by <a
                            href="https://colorlib.com" target="_blank">Colorlib</a>
                    </p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<!-- Search Begin -->

<!-- Search End -->

<!-- Js Plugins -->
<script src="{{asset('main/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('main/js/bootstrap.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('main/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('main/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('main/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('main/js/mixitup.min.js')}}"></script>
<script src="{{asset('main/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('main/js/main.js')}}"></script>
</body>

</html>


<script>
    function plus(key) {
        var quantity = $("#quantity_" + key);
        // console.log(quantity);
        var price = $("#price_" + key);

        if (quantity.val() < 10) {
            $('#quantity_' + key).val(parseInt($(quantity).val()) + 1);
            $('#total_' + key).val(parseInt($(quantity).val()) * parseInt($(price).val()) + "  so'm")
        }
    }

    function minus(key) {
        var quantity = $("#quantity_" + key);
        // console.log(quantity);
        var price = $("#price_" + key);


        if (quantity.val() > 1) {
            $('#quantity_' + key).val(parseInt($(quantity).val()) - 1);
            $('#total_' + key).val(parseInt($(quantity).val()) * parseInt($(price).val()) + "  so'm")
        }
    }

</script>








