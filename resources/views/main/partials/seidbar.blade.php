<div class="col-lg-3">
    <div class="shop__sidebar">

        <div class="shop__sidebar__accordion">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseOne">Categories</a>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="shop__sidebar__categories">
                                <table  class="order-table table"></table>
                                    <ul class="nice-scroll">
                                        @foreach($categories as $key=> $category)
                                            <li>
                                                <a
                                                   href="{{route('category.filter',$category->id)}}">{{$category->category_name}}
                                                    ({{$category_count[$key]}})</a>
                                            </li>

                                        @endforeach


                                    </ul>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseThree">Authors</a>
                    </div>
                    <div id="collapseThree" class="collapse show" data-parent="#accordionExample">
{{--                        <div class="card-body">--}}
{{--                            <div class="shop__sidebar__price">--}}
{{--                                @foreach($book_author_counts as $key=> $book_author_count)--}}
{{--                                    <ul>--}}
{{--                                        <li><a href="{{route('author.filter',$book_author_count->book_author)}}">{{$book_author_count->book_author}}--}}
{{--                                                ({{$book_author_count->count}})</a></li>--}}

{{--                                    </ul>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div id="collapsetwoo" class="collapse show " data-parent="#accordionExample1">
                            <div class="card-body">
                                <div class="shop__sidebar__categories">
                                    <ul class="nice-scroll">

                                            @foreach($book_author_counts as $key=> $book_author_count)

                                                    <li>
                                                        <a href="{{route('author.filter',$book_author_count->book_author)}}">{{$book_author_count->book_author}}
                                                            ({{$book_author_count->count}})</a></li>


                                            @endforeach


                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>


            </div>
        </div>
    </div>
</div>




