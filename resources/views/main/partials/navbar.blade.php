<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7">
                    <div class="header__top__left">
                        <p>Xush kelibsiz online kitob do'koniga</p>
                    </div>
                </div>
                @if(\Illuminate\Support\Facades\Auth::check())


                    <div class="col-lg-6 col-md-5">
                        <div class="header__top__right">
                            <div class="header__top__links">
                                <b style="color: #3b60ea"> <b style="color: #ffffff">Welcome to </b> &nbsp; {{\Illuminate\Support\Facades\Auth::user()->name }}</b> &nbsp;
                                <a style="color: red; font-size: 10px" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                    Log out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>                            </div>

                        </div>
                    </div>

                @else
                    <div class="col-lg-6 col-md-5">
                        <div class="header__top__right">
                            <div class="header__top__links">
                                <a href="{{route('login')}}">Login</a>
                                <a href="{{route('register')}}">Register</a>
                            </div>

                        </div>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="header__logo">
                    <a href="{{route('main.index')}}"><img src="{{asset('main/img/logo.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <nav class="header__menu mobile-menu">
                    <ul>
                        {{--                        <li><a href="./index.html">Home</a></li>--}}
                        <li><a href="{{route('main.index')}}">Home</a></li>
                       @auth()
                            <li><a href="{{route('view.card')}}">Card</a></li>
                       @endauth
                        <li><a href="{{route('contact')}}">Contacts</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="header__nav__option">



            </div>
        </div>
        <div class="canvas__open"><i class="fa fa-bars"></i></div>
    </div>
</header>
