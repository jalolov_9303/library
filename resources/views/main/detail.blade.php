<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
          rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('main/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/css/style.css')}}" type="text/css">
</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Offcanvas Menu Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper" >
    <div class="offcanvas__option">
        <div class="offcanvas__links">
            <a href="#">Sign in</a>
            <a href="#">FAQs</a>
        </div>
        <div class="offcanvas__top__hover">
            <span>Usd <i class="arrow_carrot-down"></i></span>
            <ul>
                <li>USD</li>
                <li>EUR</li>
                <li>USD</li>
            </ul>
        </div>
    </div>
    <div class="offcanvas__nav__option">
        <a href="#" class="search-switch"><img src="{{asset('main/img/icon/search.png')}}" alt=""></a>
        <a href="#"><img src="{{asset('main/img/icon/heart.png')}}" alt=""></a>
        <a href="#"><img src="{{asset('main/img/icon/cart.png')}}" alt=""> <span>0</span></a>
        <div class="price">$0.00</div>
    </div>
    <div id="mobile-menu-wrap"></div>
    <div class="offcanvas__text">

    </div>
</div>
<!-- Offcanvas Menu End -->

<!-- Header Section Begin -->
@include('main.partials.navbar')
<!-- Header Section End -->

<!-- Shop Details Section Begin -->
<section class="shop-details">
    <div class="product__details__pic">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product__details__breadcrumb">
                        <a href="{{route('main.index')}}">Home</a>
                        <a href="">Shop</a>
                        <span>Product Details</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <ul class="nav nav-tabs" role="tablist">
                        @for($i=0; $i<count($book_img)-1; $i++)
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-{{$i+1}}" role="tab">
                                    <div class="product__thumb__pic set-bg"
                                         data-setbg="{{asset('admin/book_images/'.$book_img[$i])}}"
                                         style="background-image: url(&quot;img/shop-details/thumb-1.png&quot;);">
                                    </div>
                                </a>
                            </li>
                        @endfor
                    </ul>
                </div>
                <div class="col-lg-6 col-md-9">
                    <div class="tab-content">
                        @for($i=0; $i<count($book_img)-1; $i++)
                            @if($i == 0)
                                <div class="tab-pane active" id="tabs-{{$i+1}}" role="tabpanel">
                                    <div class="product__details__pic__item">
                                        <img src="{{asset('admin/book_images/'.$book_img[$i])}}" alt="">
                                    </div>
                                </div>
                            @else
                                <div class="tab-pane" id="tabs-{{$i+1}}" role="tabpanel">
                                    <div class="product__details__pic__item">
                                        <img src="{{asset('admin/book_images/'.$book_img[$i])}}" alt="">
                                    </div>
                                </div>
                            @endif

                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product__details__content">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="product__details__text">
                        <h4><b>Book name: </b>{{$book->book_name}}</h4>
                        <h4><b>Book author: </b>{{$book->book_author}}</h4>
                        <h4><b>Book categories: </b>{{$book->category_name->category_name}}</h4>

                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <span> - 5 Reviews</span>
                        </div>
                        <h3><b>Book prise: </b>{{$book->book_prise}} so'm <span>{{$book->book_prise*1.3}} so'm</span>
                        </h3>
                        {{--                        <p>{{$products_accept->model_description}}</p>--}}
                        <div class="product__details__option d-flex">


                            <div class="product__details__tab">

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabs-5" role="tabpanel">
                                        <p class="note">
                                            {!! $book->book_description !!}
                                        </p>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <hr style="border: 8px solid black">

@foreach($comments as $comment)



                            <div class="col-lg-8">
                                <div class="blog__details__content">

                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="blog__details__author">
                                                <div class="blog__details__author__pic">
                                                    <img src="{{asset('main/img/blog/details/blog-author.jpg')}}" alt="">
                                                </div>
                                                <div class="blog__details__author__text">
                                                    <h5>{{$comment->user->name}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="blog__details__tags">
                                        <p>{{$comment->created_at}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog__details__btns">
                                    <div class="row">
                                        {{$comment->body}} </h5>

                                    </div>
                                </div>

                            </div>

                        @endforeach


                    </div>


                    {{--                    <div class="mb-5">--}}
                    {{--                        <h3 class="mb-4 section-title">{{$book->comments()->count()}} Izohlar</h3>--}}

                    {{--                        @foreach($book->comments as $comment)--}}

                    {{--                            <div class="media mb-4">--}}
                    {{--                                <img src="{{asset('main/img/user.jpg')}}" alt="Image"--}}
                    {{--                                     class="img-fluid rounded-circle mr-3 mt-1"--}}
                    {{--                                     style="width: 45px;">--}}
                    {{--                                <div class="media-body">--}}
                    {{--                                    <h6>{{$comment->user->name}} <small><i>{{$comment->created_at}}</i></small></h6>--}}
                    {{--                                    <p>{{$comment->body}}</p>--}}
                    {{--                                    --}}{{--                                    <button class="btn btn-sm btn-light">Reply</button>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}

                    {{--                        @endforeach--}}

                    {{--                    </div>--}}


                    <div class="col-lg-12 col-md-12">
                        <div class="contact__form">
                            <form action="{{route('comment')}}" method="Post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="hidden" name="book_id" value="{{$book->id}}">
                                    </div>

                                    <div class="col-lg-12">
                                        <textarea name="body" {{old('body')}} style="border-radius: 20px "
                                                  placeholder="Message"></textarea>
                                        <button type="submit" class="site-btn">Leave comment</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>

            </div>
        </div>
</section>


<!-- Search Begin -->
<div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch">+</div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search End -->

<!-- Js Plugins -->
<script src="{{asset('main/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('main/js/bootstrap.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('main/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('main/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('main/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('main/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('main/js/mixitup.min.js')}}"></script>
<script src="{{asset('main/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('main/js/main.js')}}"></script>
</body>

</html>
