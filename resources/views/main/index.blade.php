@extends('main.layouts.master')

@section('content')

    <div class="col-lg-9">

        <span id="total_records"></span>
        <input  type="text" name="search" id="search"  class="form-control m-2" placeholder="search">

<div class="row" id="table_data">

    @foreach($books as$key=> $book)

        <div class="col-lg-4 col-md-6 col-sm-6" >
            <div class="product__item">

                <div class="product__item__pic set-bg" data-setbg=" {{asset('admin/book_images/'.$img_for_view[$key])}} " style="background-image: url(&quot;img/product/product-2.jpg&quot;)">
                    <ul class="product__hover">

                        <li><a href="{{route('book.detail',$book)}}"><img src="{{asset('main/img/icon/search.png')}}" alt=""><span>Show</span></a></li>
                    </ul>
                </div>

                <div class="product__item__text">
                    <h6>{{$book->book_name}}</h6>

                    <a href="{{route('added.cards',$book->id)}}" class="add-cart">+ Add To Cart</a>
                    <h6><b>{{$book->book_author}}</b></h6>
                    <div class="rating">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>

                    <h5>{{$book->book_prise}} so'm  <del style="opacity: 0.4">{{$book->book_prise*1.3}} so'm</del></h5>


                </div>
            </div>
        </div>




    @endforeach
    {{--                {{ $books->currentPage() }}--}}

</div>


        <div  style="justify-content: end " class="row">
            {{ $books->links('pagination::bootstrap-4') }}

        </div>
    </div>

@endsection
@push('script')
    <script>
        $(document).ready(function ()
        {
            fetch_customer_data();
            function fetch_customer_data( query = '')
            {
                $.ajax({
                    url:"{{route('search')}}",
                    method:'GET',
                    data:{query:query},
                    dataType:'json',
                    success:function (data)
                    {
{{--                        $('#table_data').html("");--}}
{{--                        // $('tbody').html(data.table_data)--}}
{{--                        // $('#total_records').text(data.total_data);--}}
{{--                        $('#table_data').append(`--}}

{{--                               <div class="col-lg-4 col-md-6 col-sm-6" >--}}
{{--            <div class="product__item">--}}

{{--                <div class="product__item__pic set-bg" data-setbg=" {{asset('admin/book_images/'.$img_for_view[$key])}} " style="background-image: url(&quot;img/product/product-2.jpg&quot;)">--}}
{{--                    <ul class="product__hover">--}}

{{--                        <li><a href="{{route('book.detail',$book)}}"><img src="{{asset('main/img/icon/search.png')}}" alt=""><span>Show</span></a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}

{{--                <div class="product__item__text">--}}
{{--                    <h6>{{$book->book_name}}</h6>--}}

{{--                    <a href="{{route('added.cards',$book->id)}}" class="add-cart">+ Add To Cart</a>--}}
{{--                    <h6><b>{{$book->book_author}}</b></h6>--}}
{{--                    <div class="rating">--}}
{{--                        <i class="fa fa-star-o"></i>--}}
{{--                        <i class="fa fa-star-o"></i>--}}
{{--                        <i class="fa fa-star-o"></i>--}}
{{--                        <i class="fa fa-star-o"></i>--}}
{{--                        <i class="fa fa-star-o"></i>--}}
{{--                    </div>--}}

{{--                    <h5>{{$book->book_prise}} so'm  <del style="opacity: 0.4">{{$book->book_prise*1.3}} so'm</del></h5>--}}

{{--                    <div class="product__color__select">--}}
{{--                        <label for="pc-4">--}}
{{--                            <input type="radio" id="pc-4">--}}
{{--                        </label>--}}
{{--                        <label class="active black" for="pc-5">--}}
{{--                            <input type="radio" id="pc-5">--}}
{{--                        </label>--}}
{{--                        <label class="grey" for="pc-6">--}}
{{--                            <input type="radio" id="pc-6">--}}
{{--                        </label>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--`);--}}

                    }
                })
            }
            $(document).on('keyup','#search',function (){
                var query= $(this).val();
                fetch_customer_data(query);
            });


        });
    </script>
@endpush


